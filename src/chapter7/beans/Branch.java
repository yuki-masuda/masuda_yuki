package chapter7.beans;

import java.io.Serializable;

public class Branch implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String branch_name;
	public int getId() {
		return id;
	}
	public String getBranch_name() {
		return branch_name;
	}
	public void setBranch_name(String branch_name) {
		this.branch_name = branch_name;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public void setId(int id) {
		this.id = id;
	}


}