<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>登録情報更新</title>
    </head>
    <body>
        <div class="main-contents">

            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session"/>
            </c:if>

            <form action="settings" method="post"><br />
                <input name="Id" value="${edit.id}" id="id" type="hidden"/>
                <label for="login_id">ログインID</label>
                <input name="login_id" value="${edit.login_id}" id="login_id">

                <label for="name">名前</label>
                <input name="name" value="${edit.name}" id="name"/> <br />

                <label for="branch">支店</label>
                <select name="branch" size="1" id="branch" >
               		<c:forEach items="${branch}" var="branch">
		                <c:if test="${branch.id == edit.branch}">
						 <option value="${branch.id}" selected>${branch.branch_name}</option>
						</c:if>
						<c:if test="${branch.id != edit.branch}">
						 <option value="${branch.id}">${branch.branch_name}</option>
						</c:if>
                    </c:forEach>
                </select><br />

                <label for="password">パスワード</label>
                <input name="password" type="password" id="password"/> <br />

                <label for="password2">確認用パスワード</label>
                <input name="password2" type="password" id="password2"/> <br />

                <label for="position">部署・役職</label>
                <select name="position" size="1" id="position">
                	<c:forEach items="${position}" var="position">
                		<c:if test="${position.id == edit.position}">
						 <option value="${position.id}" selected>${position.position_name}</option>
						</c:if>
						<c:if test="${position.id != edit.position}">
						 <option value="${position.id}">${position.position_name}</option>
						</c:if>
                    </c:forEach>
                </select><br />

                <input type="submit" value="更新" /> <br />
                <a href="./">戻る</a>
            </form>
            <div class="copyright"> Copyright(c)Yuki Masuda</div>
        </div>
    </body>
</html>