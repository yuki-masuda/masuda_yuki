<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>掲示板</title>
    </head>
    <body>
        <div class="main-contents">
            <div class="header">
                <a href="signup">登録する</a>
            </div>
		<div class="users">
    		<c:forEach items="${userinfo}" var="users">
            	<div class="users">
                	<div class="account-name">
                    	<span class="id"><c:out value="${users.id}" /></span>
                    	<span class="name"><c:out value="${users.name}" /></span>
                    	<span class="login_id"><c:out value="${users.login_id}" /></span>
                    	<span class="branch"><c:out value="${users.branch_name}" /></span>
                    	<span class="position"><c:out value="${users.position_name}" /></span>
                    	 <form action="settings" method="get">
                    	 <input name="Id" value="${users.id}" type="hidden" />
                    	 <input type="submit" value="更新" />
                    	 </form>
                	</div>
            	</div>
    	</c:forEach>
	</div>
	<div class="copyright"> Copyright(c)YukiMasuda</div>
        </div>
    </body>
</html>